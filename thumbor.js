'use strict';

const Thumbor = require('thumbor');

function ThumborWrapper(options) {
  const that = this;
  that.debug = !!options.debug;
  that.key = options.key;
  that.url = options.url;

  /**
   * generates a signed thumbor url to any image url
   * @param imgUrl
   * @param _fitInWidth if you let it 0, then there is no limitation
   * @param _fitInHeight if you let it 0, then there is no limitation
   * @param quality default=80
   * @returns {String}
   */
  that.generateUrl = function generateUrl(imgUrl, _fitInWidth, _fitInHeight, quality) {
    if (!imgUrl) {
      return null;
    }
    const thumbor = new Thumbor(that.key, that.url);

    thumbor.setImagePath(imgUrl);
    let fitInWidth = _fitInWidth;
    let fitInHeight = _fitInHeight;

    if (!fitInWidth) {
      fitInWidth = 0;
    }
    if (!fitInHeight) {
      fitInHeight = 0;
    }
    thumbor.resize(fitInWidth, fitInHeight);

    if (quality > 0) {
      thumbor.filter(`quality(${quality})`);
    }

    return thumbor.buildUrl();
  };

  /**
   * generates a signed thumbor url to any image url
   * @param imgUrl
   * @param _fitInWidth if you let it 0, then there is no limitation
   * @param _fitInHeight if you let it 0, then there is no limitation
   * @param version default=empty is used to make versioning calls to CloudFront
   * @param quality default=80
   * @returns {String}
   */
  that.generateVersioningUrl = function generateVersioningUrl(imgUrl, _fitInWidth, _fitInHeight, version, quality) {
    if (!version || version <= 0) {
      return '';
    }

    const thumbor = new Thumbor(that.key, that.url);

    thumbor.setImagePath(imgUrl);
    let fitInWidth = _fitInWidth;
    let fitInHeight = _fitInHeight;

    if (!_fitInWidth) {
      fitInWidth = 0;
    }
    if (!_fitInHeight) {
      fitInHeight = 0;
    }
    thumbor.resize(fitInWidth, fitInHeight);

    if (quality > 0) {
      thumbor.filter(`quality(${quality})`);
    }
    thumbor.filter(`v(${version})`);
    return thumbor.buildUrl();
  };
}

/**
 * USAGE
 * https://github.com/PolicyMic/thumbor/blob/master/index.js
 * INSTALL
 * http://www.dadoune.com/blog/best-thumbnailing-solution-set-up-thumbor-on-aws/
 * @param options object {
 *   key: 'asdf...',
 *   url: 'https://',
 * }
 * @returns {ThumborWrapper}
 */
module.exports = function init(options) {
  if (!options) {
    throw new Error('ThumborWrapper.init: options parameter missing');
  }

  if (!options.key || !options.url) {
    throw new Error('ThumborWrapper.init: options { key, url } parameter missing');
  }
  return new ThumborWrapper(options || {});
};
