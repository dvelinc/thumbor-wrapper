'use strict';

const Thumbor = require('../thumbor');

const thumbor = new Thumbor({
  key: '1234',
  url: 'https://thumbor.url',
});

console.log(thumbor);
console.log('tests:');
console.log(thumbor.generateUrl('https://www.google.at/image.png'));
console.log(thumbor.generateUrl(''), 'should be null');
console.log(thumbor.generateUrl(), 'should be null');

const thumbor1 = require('../thumbor')({
  key: '1234',
  url: 'https://thumbor.url',
});

console.log(thumbor1);
console.log('tests:');
console.log(thumbor1.generateUrl('https://www.google.at/image.png'));
console.log(thumbor1.generateUrl(''), 'should be null');
console.log(thumbor1.generateUrl(), 'should be null');
